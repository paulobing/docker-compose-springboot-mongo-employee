package paulobing.dockerspringboot.service;

import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import paulobing.dockerspringboot.controller.MongoController;
import paulobing.dockerspringboot.model.BingMongoCollection;

@RestController
public class MongoService {
	private static Logger LOGGER = LoggerFactory.getLogger(MongoService.class);
	
	@Autowired
	private MongoController controller;
	
	@PostConstruct
	public void init() {
		// TODO something relevant
		LOGGER.info("INIT Service...");
	}

	@RequestMapping(value = "/mongoService/ping", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	boolean ping() {
		return true;
	}

	@RequestMapping(value = "/mongoService/collection/list/{collectionName}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	BingMongoCollection listCollection(@PathVariable("collectionName") String collectionName) {
		return controller.listCollection(collectionName);
	}

	@RequestMapping(value = "/mongoService/etc/getProperties", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	Properties getProperties() {
		return System.getProperties();
	}

	@RequestMapping(value = "/mongoService/etc/getEnv", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	Map<String, String> getEnv() {
		return System.getenv();
	}

}
