package paulobing.dockerspringboot.repository;
import org.springframework.data.mongodb.repository.MongoRepository;

import paulobing.dockerspringboot.model.Employee;
 
public interface EmployeeRepository extends MongoRepository<Employee, String> {
 
}