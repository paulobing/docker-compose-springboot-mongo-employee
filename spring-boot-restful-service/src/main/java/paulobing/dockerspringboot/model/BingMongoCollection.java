package paulobing.dockerspringboot.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BingMongoCollection {
	private String collectionName;
	private List<String> rows = new ArrayList<>();
	
	public BingMongoCollection(String collectionName) {
		this.collectionName = collectionName;
	}
	public BingMongoCollection(String collectionName, List<String> rows) {
		this(collectionName);
		this.rows = rows;
	}
	public String getCollectionName() {
		return collectionName;
	}
	public List<String> getRows() {
		return rows;
	}
	public BingMongoCollection addRow(String...newRow) {
		this.rows.addAll(Arrays.asList(newRow));
		return this;
	}
	@Override
	public String toString() {
		return "BingMongoCollection [collectionName=" + collectionName + ", rows=" + rows + "]";
	}
}
