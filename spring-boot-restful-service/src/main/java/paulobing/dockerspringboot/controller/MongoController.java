package paulobing.dockerspringboot.controller;

import org.springframework.stereotype.Component;

import paulobing.dockerspringboot.model.BingMongoCollection;

@Component
public class MongoController {
	public BingMongoCollection listCollection(String collectionName) {
		return createDummyBingMongoCollection(collectionName);
	}

	private BingMongoCollection createDummyBingMongoCollection(String collectionName) {
		return new BingMongoCollection("DummyCollection_" + collectionName).addRow("Ford", "Chevrolet", "Mazda", "Fiat");
	}

}
